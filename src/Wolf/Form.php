<?php

/**
 * This file is part of the Form package for
 * driveJohnson's Wolf. 
 *
 * Designed to be used with Bootstrap 3
 *
 * @author   Charlie Benjafield
 * @package  Form
 * @version  1.1.0
 */

namespace Wolf;

class Form 
{
	/**
	 * Form is instantiated
	 *
	 * @var bool
	 */
	protected $init = false;

	/**
	 * Fillable form attributes
	 */
	protected $formAttributes = [
		'action', 'method', 'id', 'class', 'autocomplete', 'charset'
	];

	/**
	 * Action attribute
	 *
	 * @var string
	 */
	protected $action = '';

	/**
	 * Method attribute
	 * 
	 * @var string
	 */
	protected $method = 'POST';

	/**
	 * Charset attribute
	 * 
	 * @var string
	 */
	protected $charset = 'UTF-8';

	/**
	 * ID attribute
	 * 
	 * @var string
	 */
	protected $id = '';

	/**
	 * Class attribute
	 * 
	 * @var string
	 */
	protected $class = 'form-horizontal';

	/**
	 * Autocomplete
	 * 
	 * @var bool
	 */
	protected $autocomplete = true;

	/**
	 * Store the form elements as an array.
	 *
	 * @var array
	 */
	protected $formElements = [];

	/**
	 * Store the form inputs as an array.
	 *
	 * @var array
	 */
	protected $formInputs = [];

	/**
	 * Store the hidden form inputs as an array.
	 *
	 * @var array
	 */
	protected $formHiddenInputs = [];

	/**
	 * The closing tag
	 *
	 * Acts as more of a footer than anything. Will always
	 * appear at the end of the generated form.
	 */
	protected $closingTag = '</form>';

	/**
	 * Human booleans.
	 */
	protected $humanBoolean = [
		'off', 'on'
	];

	/**
	 * Closable input types
	 *
	 * @var array
	 */
	protected $closableInputs = [
		'textarea', 'button', 'select'
	];

	/**
	 * Prefixes and Suffixes
	 */
	public $formGroupBefore = '<div class="form-group">';
	public $formGroupAfter = '</div>';
	public $labelBefore = '<label class="col-sm-3 control-label">';
	public $labelAfter = '</label>';
	public $inputBefore = '<div class="col-sm-9">';
	public $inputAfter = '</div>';
	public $inputClass = 'form-control';

	/**
	 * Create a new Form instance.
	 *
	 * @param array $params
	 */
	public function __construct($params = [])
	{
		if(!$this->init) $this->_setAttributes($params);
		$this->_makeFormTag();
	}

	/**
	 * Set form attributes
	 *
	 * @param array $params
	 */
	protected function _setAttributes($params = [])
	{
		foreach($params as $property => $value)
		{
			if(in_array($property, $this->formAttributes))
			{
				$this->$property = $value;
			}
		}
		$this->init = true;
	}

	/**
	 * Generate the form tag
	 */
	protected function _makeFormTag()
	{
		$attributes = [];
		foreach($this->formAttributes as $attr)
		{
			if(!empty($this->$attr)) $attributes[$attr] = $this->$attr;
		}
		$html = '<form ' . $this->_makeAttributes($attributes) . '>';
		$this->formElements[] = $html;
		return $html;
	}

	/**
	 * Make attributes HTML string
	 *
	 * @param array $attributes
	 */
	protected function _makeAttributes($attributes = [])
	{
		$string = [];
		foreach($attributes as $key => $value)
		{
			if(!is_bool($value)) $string[] = $key . '="' . $value . '"';
			else $string[] = $key . '="' . $this->humanBoolean[intval($value)] . '"';
		}
		return trim(implode(' ', $string));
	}

	/**
	 * Add form element.
	 *
	 * @param string $type
	 * @param string $name
	 * @param string $value
	 * @param array $attributes
	 */
	public function add($type = 'text', $name = '', $value = '', $label = '', $attributes = [])
	{
		$atts = array_merge([
			'type'  => $type,
			'name'  => $name,
			'value' => $value
		], $attributes);

		if(!isset($atts['class'])) $atts['class'] = $this->inputClass;

		$childValue = '';

		if(in_array($type, $this->closableInputs))
		{
			unset($atts['value']);
			$childValue = $value;
		}

		$html = '<' . ((in_array($type, $this->closableInputs)) ? $type : 'input') . ' ' . $this->_makeAttributes($atts) . '>' . $childValue . ((in_array($type, $this->closableInputs)) ? '</' . $type . '>' : '');
		
		$this->formInputs[] = [
			'input' => $html,
			'label' => $label
		];

		return $this;
	}

	/**
	 * Add hidden form input.
	 *
	 * @param string $name
	 * @param string $value
	 * @param array $attributes
	 */
	public function addHidden($name, $value, $attributes = [])
	{
		$atts = array_merge([
			'type'  => 'hidden',
			'name'  => $name,
			'value' => $value
		], $attributes);

		$html = '<input ' . $this->_makeAttributes($atts) . '>';

		$this->formHiddenInputs[] = $html;

		return $this;
	}

	/**
	 * Add custom HTML in the position.
	 *
	 * @param string
	 */
	public function addHtml($html = '')
	{
		$this->formInputs[] = [
			'label' => null,
			'input' => $html,
			'raw'   => true
		];

		return $this;
	}

	/**
	 * Add a select input to the form.
	 *
	 * @param string
	 * @param array
	 * @param string
	 * @param string
	 * @param array
	 */
	public function addSelect($name = '', $options = [], $default = [], $label = '', $attributes = [])
	{
		$atts = array_merge([
			'name'  => $name
		], $attributes);

		if(!isset($atts['class'])) $atts['class'] = $this->inputClass;

		$html = '<select ' . $this->_makeAttributes($atts) . '>' . $this->_makeOpts($default, $options) . '</select>';

		$this->formInputs[] = [
			'input' => $html,
			'label' => $label
		];

		return $this;
	}

	protected function _makeOpts($default = [], $opts = [])
	{
		$options = [];

		if(!empty($default)) $options[] = '<option'.((isset($default[1])) ? ' value="'.$default[1].'"' : '' ).'>' . $default[0] . '</option>';

		foreach($opts as $opt => $value) $options[] = '<option value="'.$value.'">'.$opt.'</option>';

		return implode("\n", $options);
	}

	/**
	 * Group Element
	 *
	 * @param string $input
	 */
	public function group($label = '', $input = '')
	{
		$html = $this->formGroupBefore . $this->labelBefore . $label . $this->labelAfter . $this->inputBefore . $input . $this->inputAfter . $this->formGroupAfter;
		$this->formElements[] = $html;
	}

	/**
	 * Render the form
	 *
	 * @param array
	 */
	public function render($buttons = [])
	{
		foreach($this->formInputs as $input) 
		{
			if(isset($input['raw']) && $input['raw'] == true) $this->formElements[] = $input['input'];
			else $this->group($input['label'], $input['input']);
		}

		$formFooterHtml = '<div class="hidden-fields hide-element" style="display:none;">
								'.implode("\n", $this->formHiddenInputs).'
					        </div>';

		if(!empty($buttons)) $formFooterHtml .= '<div class="form-group">
											       <div class="col-sm-9 col-sm-offset-3">
													'.$this->_makeButtons($buttons).'
											       </div>
											    </div>';

		$this->formElements[] = $formFooterHtml;
		$this->formElements[] = $this->closingTag;
		return implode("\n", $this->formElements);
	}

	/**
	 * Make the button part of the form.
	 *
	 * @param array
	 */
	protected function _makeButtons($buttons = [])
	{
		$btns = [];
		foreach($buttons as $button)
		{
			$btns[] = $button;
		}
		return implode($btns);
	}

}